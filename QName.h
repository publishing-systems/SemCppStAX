/* Copyright (C) 2017-2021 Stephan Kreutzer
 *
 * This file is part of SemCppStAX.const 
 *
 * SemCppStAX is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SemCppStAX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SemCppStAX. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/QName.h
 * @author Stephan Kreutzer
 * @since 2017-08-24
 */

#ifndef _SEMCPPSTAX_QNAME
#define _SEMCPPSTAX_QNAME

#include <memory>
#include <string>

namespace cppstax
{

class QName
{
public:
    QName(std::unique_ptr<std::string>& pNamespaceUri,
          std::unique_ptr<std::string>& pLocalPart,
          std::unique_ptr<std::string>& pPrefix);

public:
    const std::string& getNamespaceURI() const;
    const std::string& getLocalPart() const;
    const std::string& getPrefix() const;

public:
    bool operator==(const QName& rhs) const;
    bool operator!=(const QName& rhs) const;

protected:
    std::unique_ptr<std::string> m_pNamespaceUri;
    std::unique_ptr<std::string> m_pLocalPart;
    std::unique_ptr<std::string> m_pPrefix;

};

}

#endif
