/* Copyright (C) 2017-2021 Stephan Kreutzer
 *
 * This file is part of SemCppStAX.
 *
 * SemCppStAX is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SemCppStAX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SemCppStAX. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/QName.cpp
 * @author Stephan Kreutzer
 * @since 2017-08-24
 */

#include "QName.h"

namespace cppstax
{

QName::QName(std::unique_ptr<std::string>& pNamespaceUri,
             std::unique_ptr<std::string>& pLocalPart,
             std::unique_ptr<std::string>& pPrefix):
  m_pNamespaceUri(std::move(pNamespaceUri)),
  m_pLocalPart(std::move(pLocalPart)),
  m_pPrefix(std::move(pPrefix))
{
    if (m_pNamespaceUri == nullptr)
    {
        throw new std::invalid_argument("Nullptr passed.");
    }

    if (m_pLocalPart == nullptr)
    {
        throw new std::invalid_argument("Nullptr passed.");
    }

    if (m_pPrefix == nullptr)
    {
        throw new std::invalid_argument("Nullptr passed.");
    }
}

const std::string& QName::getNamespaceURI() const
{
    return *m_pNamespaceUri;
}

const std::string& QName::getLocalPart() const
{
    return *m_pLocalPart;
}
const std::string& QName::getPrefix() const
{
    return *m_pPrefix;
}

bool QName::operator==(const QName& rhs) const
{
    return *m_pLocalPart == rhs.getLocalPart() &&
           *m_pPrefix == rhs.getPrefix() &&
           *m_pNamespaceUri == rhs.getNamespaceURI();
}

bool QName::operator!=(const QName& rhs) const
{
    return !(*this == rhs);
}

}
