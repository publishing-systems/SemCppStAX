/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of SemCppStAX.
 *
 * SemCppStAX is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SemCppStAX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SemCppStAX. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Namespace.cpp
 * @author Stephan Kreutzer
 * @since 2021-04-02
 */

#include "Namespace.h"

namespace cppstax
{

Namespace::Namespace(std::unique_ptr<std::string>& pUri,
                     std::unique_ptr<std::string>& pPrefix,
                     bool bIsDefaultNamespaceDeclaration):
  m_pUri(std::move(pUri)),
  m_pPrefix(std::move(pPrefix)),
  m_bIsDefaultNamespaceDeclaration(bIsDefaultNamespaceDeclaration)
{
    if (m_pUri == nullptr)
    {
        throw new std::invalid_argument("Nullptr passed.");
    }

    if (m_pPrefix == nullptr)
    {
        throw new std::invalid_argument("Nullptr passed.");
    }
}

const std::string& Namespace::getNamespaceURI() const
{
    return *m_pUri;
}

const std::string& Namespace::getPrefix() const
{
    return *m_pPrefix;
}

bool Namespace::isDefaultNamespaceDeclaration() const
{
    return m_bIsDefaultNamespaceDeclaration;
}

}
