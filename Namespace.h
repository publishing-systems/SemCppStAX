/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of SemCppStAX.
 *
 * SemCppStAX is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * SemCppStAX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with SemCppStAX. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Namespace.h
 * @author Stephan Kreutzer
 * @since 2021-04-02
 */

#ifndef _SEMCPPSTAX_NAMESPACE_H
#define _SEMCPPSTAX_NAMESPACE_H

#include <memory>
#include <string>

namespace cppstax
{

class Namespace
{
public:
    Namespace(std::unique_ptr<std::string>& pUri,
              std::unique_ptr<std::string>& pPrefix,
              bool bIsDefaultNamespaceDeclaration);

    const std::string& getNamespaceURI() const;
    const std::string& getPrefix() const;
    bool isDefaultNamespaceDeclaration() const;

protected:
    std::unique_ptr<std::string> m_pUri;
    std::unique_ptr<std::string> m_pPrefix;
    bool m_bIsDefaultNamespaceDeclaration;

};

}

#endif
